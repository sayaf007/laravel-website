<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{ 
  public $fillable = [
    'name',
    'address',
    'email',
    'phone_no'
  ];

    public function products()
    {
      return $this->hasMany(Product::class);
    }
}
