<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\product;
use App\Models\company;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(){
        $products = Product::orderBy('id', 'desc')->paginate(6);
        return view('Frontend.pages.product.index')->with('products', $products);
    }

    public function show($slug){
      $product = Product::where('slug', $slug)->first();
      if(!is_null($product)){
        return view('Frontend.pages.product.show', compact('product'));
      }else{
          session()->flash('error', 'Sorry there is no product by this url');
          return redirect()->route('products');
      }

    }
}
