<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Product;

use App\Models\ProductImage;


use Image;

class ProductsController extends Controller
{   
    
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {   
       $products = Product::orderBy('id', 'desc')->get();
       return view('Backend.pages.products.index')->with('products', $products);
    }
    

    public function create()
    {
       return view('Backend.pages.products.create');
    }

    public function edit($id)
    {   
        $product = Product::find($id);
       return view('Backend.pages.products.edit')->with('product', $product);
    }

   

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'title' => 'required|max:155',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'brand_id' => 'required|numeric',
            'category_id' => 'required|numeric',
            'company_id' => 'required|numeric',
          
        ]);

       $product = new Product;
       $product->title = $request->title;
       $product->description = $request->description;
       $product->price = $request->price;
       $product->quantity = $request->quantity;
   
       $product->slug = str_slug($request->title);
       $product->company_id = $request->company_id;
       $product->category_id = $request->category_id;
       $product->brand_id = $request->brand_id;
       $product->admin_id = 1;
       $product->offer_price = 0;
       $product->save();
       //Insert image into ProductImage model

       if(count($request->product_image) > 0){
        foreach ($request->product_image as $image ) {
            $img = time() . '.'. $image->getClientOriginalExtension();
            $location = public_path('image/products/' .$img);
            Image::make($image)->save($location);
            
            $product_image = new ProductImage;
            $product_image->product_id = $product->id;
            $product_image->image = $img;
            $product_image->save();
        }
       }
       return redirect()->route('admin.product.create');
    }

    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'title' => 'required|max:155',
            'description' => 'required',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
            'brand_id' => 'required|numeric',
            'category_id' => 'required|numeric',
            'company_id' => 'required|numeric',
            
        ]);

       $product = Product::find($id);
       $product->title = $request->title;
       $product->description = $request->description;
       $product->price = $request->price;
       $product->quantity = $request->quantity;
       $product->company_id = $request->company_id;
       $product->category_id = $request->category_id;
       $product->brand_id = $request->brand_id;
       $product->save();

       //Insert image into ProductImage model


       return redirect()->route('admin.products');
    }

    public function delete($id)
    {   
        $product = Product::find($id);

        if(!is_null($product)){
            $product->delete();
        }
    session()->flash('success', 'Product has deleted successfully !!');
    return back();
    }
}
