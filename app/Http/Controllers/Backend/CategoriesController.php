<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Image;
use File;

use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }
    
   public function index()
   {
       $categories = Category::orderBy('id', 'desc')->get();
       return view('Backend.pages.categories.index', compact('categories'));
   }

   public function create()
   {
       $main_categories = Category::orderBy('name', 'desc')->where('parent_id', 0)->get();
       return view('Backend.pages.categories.create', compact('main_categories'));
   }

   public function store(Request $request)
   {
     $this->validate($request, [
          'name' => 'required',
          'image' => 'nullable|image',
      ],
      [
        'name.required' => 'Name can not be empty',
        'image.image' => 'please add an image',
      ]);

      $category = new Category();
      $category->name = $request->name;
      $category->description = $request->description;
      $category->parent_id = $request->parent_id;
      
    //   if(count($request->image) > 0){
        $image = $request->file('image');
        if ($image > 0){
        $img = time() . '.'. $image->getClientOriginalExtension();
        $location = public_path('image/categories/' .$img);
        Image::make($image)->save($location);
        $category->image = $img;
        }
    // }
   

      $category->save();

      session()->flash('success', 'A new category has been added successfully !!');
      return redirect()->route('admin.categories');
      
   }

   public function edit($id)
   {
    $main_categories = Category::orderBy('name', 'desc')->where('parent_id', 0)->get();
    $category = Category::find($id);
    if (!is_null($category)) {
        return view('Backend.pages.categories.edit', compact('category', 'main_categories'));
    } else {
        return redirect()->route('admin.categories');
    }
    
   }

   public function update(Request $request, $id)
   {
     $this->validate($request, [
          'name' => 'required',
          'image' => 'nullable|image',
      ],
      [
        'name.required' => 'Name can not be empty',
        'image.image' => 'please add an image',
      ]);

      $category = Category::find($id);
      $category->name = $request->name;
      $category->description = $request->description;
      $category->parent_id = $request->parent_id;
      $image = $request->file('image');
      if ($image > 0){
        //Delete the old image

        if (File::exists('image/categories/'.$category->image)){
            File::delete('image/categories/'.$category->image);
        }
        
       
        $img = time() . '.'. $image->getClientOriginalExtension();
        $location = public_path('image/categories/' .$img);
        Image::make($image)->save($location);
        $category->image = $img;
      //  }
    }
    $category->save();

    session()->flash('success', 'A new category has been updated successfully !!');
    return redirect()->route('admin.categories');

  }

  public function delete($id)
  {   
      $category = Category::find($id);

      if(!is_null($category)){
        //if it is parent category then delete al of its sub category
        if ($category->parent_id == NULL) {
            //delete its sub category
            $sub_categories = Category::orderBy('name', 'desc')->where('parent_id', $category->id)->get();
            foreach ($sub_categories as $sub) {
                if (File::exists('image/categories/'.$sub->image)){
                    File::delete('image/categories/'.$sub->image);
                }
                $sub->delete();
            }
        }
        //Delete category image
        if (File::exists('image/categories/'.$category->image)){
            File::delete('image/categories/'.$category->image);
        }
          $category->delete();
      }
  session()->flash('success', 'Category has deleted successfully !!');
  return back();
  }
   
}
