<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Models\Company;

use Illuminate\Http\Request;
use Image;
use File;

class CompanyController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    
   public function index()
   {
       $companies = Company::orderBy('id', 'desc')->get();
       return view('Backend.pages.companies.index', compact('companies'));
   }

   public function create()
  {
    return view('backend.pages.companies.create');
  }
  

   public function store(Request $request)
   {
     $this->validate($request, [
          'name' => 'required',
          'address' => 'required',
          'email' => 'required',
          'phone_no' => 'required',
          
      ],
      [
        'name.required' => 'Name can not be empty',
        'address.required' => 'address can not be empty',
        'email.required' => 'email can not be empty',
        'phone_no.required' => 'phone_no can not be empty',
      ]);

      $Company = new Company();
      $Company->name = $request->name;
      $Company->address = $request->address;
      $Company->email = $request->email;
      $Company->phone_no = $request->phone_no;
   
      $Company->save();

      session()->flash('success', 'A new Company has been added successfully !!');
      return redirect()->route('admin.companies');
      
   }

   public function edit($id)
   {
    $Company = Company::find($id);
    if (!is_null($Company)) {
        return view('Backend.pages.companies.edit', compact('Company'));
    } else {
        return redirect()->route('admin.companies');
    }
    
   }

   public function update(Request $request, $id)
   {
     $this->validate($request, [
        'name' => 'required',
        'address' => 'required',
        'email' => 'required',
        'phone_no' => 'required',
      ],
      [
        'name.required' => 'Name can not be empty',
        'address.required' => 'address can not be empty',
        'email.required' => 'email can not be empty',
        'phone_no.required' => 'phone_no can not be empty',
      ]);

      $Company = Company::find($id);
      $Company->name = $request->name;
      $Company->address = $request->address;
      $Company->email = $request->email;
      $Company->phone_no = $request->phone_no;
      
      $Company->save();

    session()->flash('success', 'Company has been updated successfully !!');
    return redirect()->route('admin.companies');

  }

  public function delete($id)
  {   
      $Company = Company::find($id);

      if(!is_null($Company)){
        //if it is parent Company then delete al of its sub Company
          $Company->delete();
      }
  session()->flash('success', 'Company has deleted successfully !!');
  return back();
  }
}
