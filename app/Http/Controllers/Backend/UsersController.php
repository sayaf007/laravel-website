<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\User;



class UsersController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {   
       $users = User::orderBy('id', 'desc')->get();
       return view('Backend.pages.users.index')->with('users', $users);
    }
   


    public function update(Request $request, $id)
    {

        $validatedData = $request->validate([
            'status' => 'required|max:155',
        ]);

       $user = User::find($id);
       $user->status = $request->status;
       $user->save();

       //Insert image into userImage model


       return redirect()->route('admin.users');
    }

    public function delete($id)
    {   
        $user = User::find($id);

        if(!is_null($user)){
            $user->delete();
        }
    session()->flash('success', 'user has deleted successfully !!');
    return back();
    }
}
