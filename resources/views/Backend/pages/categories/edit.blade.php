@extends('Backend.layouts.master');

@section('content')
<div class="main-panel">
    <div class="content-wrapper">

     <div class="card">
       <div class="card-header">
         Edit Category
       </div>
       <div class="card-body">
        <form action="{{route('admin.category.update', $category->id)}}" method="post" enctype="multipart/form-data">
          @include('Backend.partials.messages')
          @csrf
                <div class="form-group">
                  <label for="exampleInputEmail1">Name</label>
                  <input type="text" name="name" class="form-control" id="name"  value="{{$category->name}}">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Description (optional)</label>
                  <textarea name="description" rows="8" cols="80" class="form-control">{!!$category->description!!}</textarea>
                </div>
  
                <div class="form-group">
                  <label for="exampleInputPassword1">Parent Category (optional)</label>
                  <select class="form-control" name="parent_id">
                    <option value="">Please select a primary category</option>
                    @foreach ($main_categories as $cat)
                      <option value="{{ $cat->id }}" {{ $cat->id == $category->parent_id ? 'selected' : ''}}>{{ $cat->name }}</option>
                    @endforeach
                  </select>
    
                </div>
                
                <div class="form-group">
                   <label for="image">Category old Image</label>
                   <img src="{!!asset('image/categories/'.$category->image)!!}" width="100"><br>

                   <label for="newimage">Category new Image (optional)</label>
                   <input type="file" name="image" class="form-control" id="image" >
                        
                </div>
                   
                <button type="submit" class="btn btn-success">Update Category</button>
              </form>
       </div>
     </div>

    </div>
  </div>
  <!-- main-panel ends -->
@endsection