@extends('Backend.layouts.master');

@section('content')
<div class="main-panel">
    <div class="content-wrapper">

     <div class="card">
       <div class="card-header">
         Add category
       </div>
       <div class="card-body">
       <form action="{{route('admin.category.store')}}" method="post" enctype="multipart/form-data">
        @include('Backend.partials.messages')
        @csrf
              <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="text" name="name" class="form-control" id="name"  placeholder="Enter category name">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <textarea name="description" rows="8" cols="80" class="form-control" placeholder="Enter category description"></textarea>
              </div>

              <div class="form-group">
                <label for="exampleInputPassword1">Parent Category</label>
                <select class="form-control" name="parent_id">
                  <option value=0>Please select a parent category</option>
                  @foreach ($main_categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
  
              </div>
              
              <div class="form-group">
                 <label for="image">Category Image</label>
                 <input type="file" name="image" class="form-control" id="image" >
                      
              </div>
                 
              <button type="submit" class="btn btn-primary">Add Category</button>
            </form>
       </div>
     </div>

    </div>
  </div>
  <!-- main-panel ends -->
@endsection