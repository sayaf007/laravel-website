@extends('backend.layouts.master')

@section('content')
  <div class="main-panel">
    <div class="content-wrapper">

      <div class="card">
        <div class="card-header">
          Edit Company
        </div>
        <div class="card-body">
          <form action="{{ route('admin.Company.update', $Company->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('backend.partials.messages')
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" value="{{ $Company->name }}">
            </div>

            <div class="form-group">
                <label for="name">Address</label>
                <input type="text" class="form-control" name="address" id="name" aria-describedby="emailHelp" value="{{ $Company->address }}">
            </div>

            <div class="form-group">
                <label for="name">Email</label>
                <input type="text" class="form-control" name="email" id="name" aria-describedby="emailHelp" value="{{ $Company->email }}">
            </div>

            <div class="form-group">
                <label for="name">Phone Number</label>
                <input type="text" class="form-control" name="phone_no" id="name" aria-describedby="emailHelp" value="{{ $Company->phone_no }}">
            </div>

            <button type="submit" class="btn btn-success">Update Company</button>
          </form>
        </div>
      </div>

    </div>
  </div>
  <!-- main-panel ends -->
@endsection
