@extends('backend.layouts.master')

@section('content')
  <div class="main-panel">
    <div class="content-wrapper">

      <div class="card">
        <div class="card-header">
          Add Company
        </div>
        <div class="card-body">
          <form action="{{ route('admin.Company.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('backend.partials.messages')
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" class="form-control" name="name" id="name" aria-describedby="emailHelp" placeholder="Enter Company Name">
            </div>

            <div class="form-group">
              <label for="exampleInputPassword1">Address</label>
              <input type="text" class="form-control" name="address" id="name" aria-describedby="emailHelp" placeholder="Enter Company Address">            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="text" class="form-control" name="email" id="name" aria-describedby="emailHelp" placeholder="Enter Company email">
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Phone Number</label>
                <input type="text" class="form-control" name="phone_no" id="name" aria-describedby="emailHelp" placeholder="Enter Company Name">
            </div>


            


            <button type="submit" class="btn btn-primary">Add Company</button>
          </form>
        </div>
      </div>

    </div>
  </div>
  <!-- main-panel ends -->
@endsection
