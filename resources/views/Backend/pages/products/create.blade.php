@extends('Backend.layouts.master');

@section('content')
<div class="main-panel">
    <div class="content-wrapper">

     <div class="card">
       <div class="card-header">
         Add product
       </div>
       <div class="card-body">
       <form action="{{route('admin.product.store')}}" method="post" enctype="multipart/form-data">
        @include('Backend.partials.messages')
        {{csrf_field()}}
              <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" name="title" class="form-control" id="exampleInputEmail1"  placeholder="Enter title">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Description</label>
                <textarea name="description" rows="8" cols="80" class="form-control" placeholder="Write product description"></textarea>
              </div>
              <div class="form-group">
                  <label for="exampleInputEmail1">Price</label>
                  <input type="number" name="price" class="form-control" id="exampleInputEmail1"  placeholder="Enter price">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Quantity</label>
                    <input type="number" name="quantity" class="form-control" id="exampleInputEmail1"  placeholder="Enter quantity">
                  </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Select Company</label>
                        <select class="form-control" name="company_id">
                          <option value="">Please select a company for the product</option>
                            @foreach (App\Models\Company::orderBy('name', 'asc')->get() as $company)
                              <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                      </div>
                  <div class="form-group">
                      <label for="exampleInputEmail1">Select Category</label>
                      <select class="form-control" name="category_id">
                        <option value="">Please select a category for the product</option>
                        @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', 0)->get() as $parent)
                          <option value="{{ $parent->id }}">{{ $parent->name }}</option>
        
                          @foreach (App\Models\Category::orderBy('name', 'asc')->where('parent_id', $parent->id)->get() as $child)
                            <option value="{{ $child->id }}"> ------> {{ $child->name }}</option>
        
                          @endforeach
        
                        @endforeach
                      </select>
                    </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Select Brand</label>
                    <select class="form-control" name="brand_id">
                      <option value="">Please select a brand for the product</option>
                        @foreach (App\Models\Brand::orderBy('name', 'asc')->get() as $brand)
                          <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                        @endforeach
                    </select>
                  </div>
                <div class="form-group">
                      <label for="product_image">Product Image</label>
                     <div class="row">
                       <div class="col-md-4">
                          <input type="file" name="product_image[]" class="form-control" id="product_image" >
                       </div>
                       <div class="col-md-4">
                          <input type="file" name="product_image[]" class="form-control" id="product_image" >
                       </div>
                       <div class="col-md-4">
                          <input type="file" name="product_image[]" class="form-control" id="product_image" >
                       </div>
                       <div class="col-md-4">
                          <input type="file" name="product_image[]" class="form-control" id="product_image" >
                       </div>
                       <div class="col-md-4">
                          <input type="file" name="product_image[]" class="form-control" id="product_image" >
                       </div>
                     </div>
                    </div>
                 
              <button type="submit" class="btn btn-primary">Add Product</button>
            </form>
       </div>
     </div>

    </div>
  </div>
  <!-- main-panel ends -->
@endsection