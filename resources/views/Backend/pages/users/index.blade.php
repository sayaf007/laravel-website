@extends('Backend.layouts.master');

@section('content')
<div class="main-panel">
    <div class="content-wrapper">

     <div class="card">
       <div class="card-header">
         Manage Users
       </div>

       <div class="table-responsive">
          @include('Backend.partials.messages')
        <table class="table table-hover table-striped">
          <thead>
           <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Phone No</th>
            <th scope="col">Country</th>
            <th scope="col">Company Name</th>
            <th scope="col">Payment</th>
            <th scope="col">Transection Id</th>
            <th scope="col">Approve Date</th>
            <th scope="col">Approve or Delete</th>
           </tr>
          </thead>

            @foreach ($users as $user)
            <tbody>
                <tr scope="row">
                        <td>#</td>
                        <td>{{$user->first_name}} {{$user->last_name}} </td>
                        <td>{{$user->phone_no}}</td>
                        <td>{{$user->country}}</td>
                        <td>{{$user->company_name}}</td>
                        <td>{{$user->payment}}</td>
                        <td>{{$user->transectionId}}</td>
                        <td>
                          @if ($user->status !== 2)
                              Not approved yet
                          @else
                            {{$user->updated_at}}
                          @endif
                        </td>
                        
                <td>
                    <form action="{!! route('admin.users.update', $user->id) !!}" method="post" >
                        {{ csrf_field() }}
                          <button name="status" value="2" type="submit" class="btn btn-success {{ $user->status == 2 ? 'd-none' : '' }}" >Approve</button>
                      </form>
                <a href="#deleteModal{{ $user->id }}" data-toggle="modal" class="btn btn-danger">Delete</a>

                  <!-- Modal -->
                  <div class="modal fade" id="deleteModal{{ $user->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <form action="{!! route('admin.users.delete', $user->id) !!}" method="post" >
                              {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger" >Permanent Delete</button>
                            </form>
                            
                        </div>
                        <div class="modal-footer">
                         
                          <button type="button" class="btn btn-primary" data-dismiss="modal">cancel</button>
                        </div>
                      </div>
                    </div>
                  </div>

                </td>
       
                </tr>
              </tbody>
            @endforeach

        </table>

       </div>

     </div>

    </div>
  </div>
  <!-- main-panel ends -->
@endsection