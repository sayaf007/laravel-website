<nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
      
      
          {{-- <a class="navbar-brand" href="{{ route('home') }}">LaraEcommerce</a> --}}
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
    
          <ul class="navbar-nav ml-auto">
            <li class="navbar-brand mt-1">
              <form class="form-inline my-2 my-lg-0" action="{!! route('search') !!}" method="get">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" name="search" placeholder="Search Products" aria-label="Recipient's username" aria-describedby="basic-addon2">
                  <div class="form-group">
                      <button class="btn btn-primary" type="submit">Search</button>
                    </div>
                </div>
    
              </form>
            </li>
              <a class="navbar-brand mt-2" href="{{ route('home') }}">Home</a>
                <li class="nav-item dropdown">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="{{ App\Helpers\ImageHelper::getUserImage(Auth::user()->id) }}" class="img rounded-circle" style="width:40px">
                    {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                    <span class="caret"></span>
                  </a>
      
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Logout
                  </a>
      
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                  </form>
                </div>
              </li>
          
          </ul>
      
        </div>
      </div>
      </nav>
      <!-- End Navbar Part -->
      