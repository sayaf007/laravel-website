@extends('frontend.layouts.master')

@section('nav')
@include('frontend.partials.nav')
@endsection

@section('content')
<div class="container">
  
<div style="margin-top: 150px; height:100px">
  <h1 class="display-4 offset-4">
    Search Products
  </h1>
</div>
<div class="col-sm-10 col-md offset-3">
    <form class="form-inline my-2 my-lg-0" action="{!! route('search') !!}" method="get">
      {{-- <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button> --}}
      <div class="form-group row">
        <input type="text" class="form-control" style="width: 550px;" name="search" placeholder="Search Products" aria-label="Recipient's username" aria-describedby="basic-addon2">
        <div class="form-group">
          <button class="btn btn-primary" type="submit">Search</button>
        </div>
      </div>

    </form>
  </div>

</div>
@endsection