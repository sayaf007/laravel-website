<div class="row">
  @foreach ($products as $product)
    <div class="col-md-3 p-3">
      <div class="card p-1">
        {{-- <img class="card-img-top feature-img" src="{{ asset('images/products/'. 'galaxy.png') }}" alt="Card image" > --}}
        @php $i = 1; @endphp

        @foreach ($product->images as $image)
          @if ($i > 0)
              <a href="{!! route('products.show', $product->slug) !!}">
                <img style="height: 200px" class="card-img-top feature-img" src="{{ asset('image/products/'. $image->image) }}" alt="{{ $product->title }}" >
              </a>
          @endif

          @php $i--; @endphp
        @endforeach

        <div class="card-body">
          <h4 class="card-title">
            <a href="{!! route('products.show', $product->slug) !!}">{{ $product->title }}</a>
          </h4>
          <p class="card-text">Taka - {{ $product->price }}</p>
          
        
        </div>
      </div>
    </div>

  @endforeach

</div>
<div class="mt-4 pagination">
  {{ $products->links() }}
</div>
