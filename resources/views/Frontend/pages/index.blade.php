@extends('frontend.layouts.master')

@section('content')

<div class="container">
  <div class="d-flex justify-content-center">
      <div class="p-3">
         <h1 class="display-1">Welcome</h1>
       </div>
  </div>

  <div class="d-flex justify-content-center">
      <div class="p-3">
          <a href="{{ route('login') }}"><button type="button" class="btn btn-primary">Login</button></a>
          <a href="{{ route('register') }}"><button type="button" class="btn btn-danger">Register</button></a>
      </div>
  </div>


    
@endsection