@extends('frontend.layouts.master')

@section('nav')
@include('frontend.partials.nav2')
@endsection

@section('content')

  <!-- Start Sidebar + Content -->
  <div class='container margin-top-20'>
    <div class="row">
      <div class="col-md-12">
        <div class="widget">
          <h3> Searched Products For - <span class="badge badge-primary">{{ $search }}</span></h3>
          @include('frontend.pages.all_products')
        </div>
        <div class="widget">

        </div>
      </div>


    </div>
  </div>

  <!-- End Sidebar + Content -->
@endsection
