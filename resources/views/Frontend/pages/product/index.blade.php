@extends('frontend.layouts.master')

@section('content')

<! -- start sidebar + content Part -->
<div class='container'>
    <div class="row">
        <div class="col-md-4 margin-top-20">
            @include('frontend.partials.product-sidebar')
          </div>

        <div class="col-md-8">
          <div class="widget">
            <h3>All Products</h3>
            @include('frontend.pages.product.partials.all_products')
          </div>
          <div class="widget">

          </div>
        </div>
    </div>
</div>

<! -- End sidebar + content Part -->


    
@endsection