@extends('frontend.layouts.master')

@section('content')
  <div class="container mt-2">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Register</div>

          <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
              @csrf

              <div class="form-group row">
                <label for="first_name" class="col-md-4 col-form-label text-md-right">First Name</label>

                <div class="col-md-6">
                  <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>

                  @if ($errors->has('first_name'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="last_name" class="col-md-4 col-form-label text-md-right">Last Name</label>

                <div class="col-md-6">
                  <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>

                  @if ($errors->has('last_name'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="company_name" class="col-md-4 col-form-label text-md-right">Company Name</label>

                <div class="col-md-6">
                  <input id="company_name" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{ old('company_name') }}" required>

                  @if ($errors->has('company_name'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('company_name') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              
              <div class="form-group row">
                <label for="country" class="col-md-4 col-form-label text-md-right">Country</label>

                <div class="col-md-6">
                  <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ old('country') }}" required>

                  @if ($errors->has('country'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('country') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>

                <div class="col-md-6">
                  <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                  @if ($errors->has('email'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="phone_no" class="col-md-4 col-form-label text-md-right">Phone No</label>

                <div class="col-md-6">
                  <input id="phone_no" type="text" class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" name="phone_no" value="{{ old('phone_no') }}" required>

                  @if ($errors->has('phone_no'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('phone_no') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                <div class="col-md-6">
                  <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>
              </div>

              <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                <div class="col-md-6">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
              </div>

              <div class="form-group row">
                <label for="payment_method" class="col-md-4 col-form-label text-md-right">Select a payment method</label>
      
                <div class="col-md-6">
                  <select class="form-control" name="payment" required id="payments">
                    <option value="">Select a payment method please</option>
                    @foreach ($payments as $payment)
                      <option value="{{ $payment->short_name }}">{{ $payment->name }}</option>
                    @endforeach
                  </select>
      
                  @foreach ($payments as $payment)
                    @if ($payment->short_name == "cash_in")
                      <div id="payment_{{ $payment->short_name }}" class="alert alert-success mt-2 text-center hidden">
                        <h3>
                          For Cash in there is nothing necessary. Just click Finish Order.
                          <br>
                          <small>
                            You will get your product in two or three business days.
                          </small>
                        </h3>
                      </div>
                    @else
                      <div id="payment_{{ $payment->short_name }}" class="alert alert-success mt-2 text-center hidden">
                        <h3>{{ $payment->name }} Payment</h3>
                        <p>
                          <strong>{{ $payment->name }} No :  {{ $payment->no }}</strong>
                          <br>
                          <strong>Account Type: {{ $payment->type }}</strong>
                        </p>
                        <div class="alert alert-success">
                          Please send the above money to this Bkash No and write your transaction code below there..
                        </div>
      
                      </div>
                    @endif
                  @endforeach
                  <input type="text" name="transectionId" id="transaction_id" class="form-control hidden" placeholder="Enter transaction id">
                </div>
              </div>
              
              <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                  <button type="submit" class="btn btn-primary">
                    Register
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
  $("#payments").change(function(){
    $payment_method = $("#payments").val();
    if ($payment_method == "bkash") {
      $("#payment_bkash").removeClass('hidden');
      $("#payment_cash_in").addClass('hidden');
      $("#payment_rocket").addClass('hidden');
      $("#transaction_id").removeClass('hidden');
    }else if ($payment_method == "rocket") {
      $("#payment_rocket").removeClass('hidden');
      $("#payment_bkash").addClass('hidden');
      $("#payment_cash_in").addClass('hidden');
      $("#transaction_id").removeClass('hidden');
    }
  })
  </script>
@endsection