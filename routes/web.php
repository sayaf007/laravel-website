<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\PagesController@index')->name('index');
Route::get('/home', 'Frontend\PagesController@home')->name('home');

Route::get('/contact', 'Frontend\PagesController@contact')->name('contact');

/*
Product Routes
All the routes for our product for frontend
*/

Route::group(['prefix' => 'products'], function(){
    
    Route::get('/{slug}', 'Frontend\ProductsController@show')->name('products.show');
    Route::get('/new/search', 'Frontend\PagesController@search')->name('search');
  
    //Category Routes
    Route::get('/categories', 'Frontend\CategoriesController@index')->name('categories.index');
    Route::get('/category/{id}', 'Frontend\CategoriesController@show')->name('categories.show');
});

  // User Routes
  Route::group(['prefix' => 'user'], function(){
    Route::get('/token/{token}', 'Frontend\VerificationController@verify')->name('user.verification');
    Route::get('/dashboard', 'Frontend\UsersController@dashboard')->name('user.dashboard');
    Route::get('/profile', 'Frontend\UsersController@profile')->name('user.profile');
    Route::post('/profile/update', 'Frontend\UsersController@profileUpdate')->name('user.profile.update');
});

// Cart Routes
Route::group(['prefix' => 'carts'], function(){
    Route::get('/', 'Frontend\CartsController@index')->name('carts');
    Route::post('/store', 'Frontend\CartsController@store')->name('carts.store');
    Route::post('/update/{id}', 'Frontend\CartsController@update')->name('carts.update');
    Route::post('/delete/{id}', 'Frontend\CartsController@destroy')->name('carts.delete');
});

// Checkout Routes
Route::group(['prefix' => 'checkout'], function(){
    Route::get('/', 'Frontend\CheckoutsController@index')->name('checkouts');
    Route::post('/store', 'Frontend\CheckoutsController@store')->name('checkouts.store');
});

// Admin Routes
Route::group(['prefix' => 'admin'], function(){
    Route::get('/', 'Backend\PagesController@index')->name('admin.index');
  
    // Admin Login Routes
    Route::get('/login', 'Auth\Admin\LoginController@showLoginForm')->name('admin.login');
    Route::post('/login/submit', 'Auth\Admin\LoginController@login')->name('admin.login.submit');
    Route::post('/logout/submit', 'Auth\Admin\LoginController@logout')->name('admin.logout');
    
    //Forgot password email sent
    Route::get('/password/reset', 'Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('/password/resetPost', 'Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');

     //password reset
     Route::get('/password/reset/{token}', 'Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');
     Route::post('/password/reset', 'Auth\Admin\ResetPasswordController@reset')->name('admin.password.reset.post');
 
    
    //Product Routes
    Route::group(['prefix' => '/products'], function () {
        Route::get('/', 'Backend\ProductsController@index')->name('admin.products');
        Route::get('/create', 'Backend\ProductsController@create')->name('admin.product.create');
        Route::get('/edit/{id}', 'Backend\ProductsController@edit')->name('admin.product.edit');
        Route::post('/delete/{id}', 'Backend\ProductsController@delete')->name('admin.product.delete');
        Route::post('/store', 'Backend\ProductsController@store')->name('admin.product.store');
        Route::post('/edit/{id}', 'Backend\ProductsController@update')->name('admin.product.update');
    });

    //User control
    Route::group(['prefix' => '/users'], function () {
        Route::get('/', 'Backend\UsersController@index')->name('admin.users');
        Route::post('/delete/{id}', 'Backend\UsersController@delete')->name('admin.users.delete');
        Route::post('/approve/{id}', 'Backend\UsersController@update')->name('admin.users.update');
    });

    //Category Routes
    Route::group(['prefix' => '/categories'], function () {
        Route::get('/', 'Backend\CategoriesController@index')->name('admin.categories');
        Route::get('/create', 'Backend\CategoriesController@create')->name('admin.category.create');
        Route::get('/category/edit/{id}', 'Backend\CategoriesController@edit')->name('admin.category.edit');
        Route::post('/delete/{id}', 'Backend\CategoriesController@delete')->name('admin.category.delete');
        Route::post('/store', 'Backend\CategoriesController@store')->name('admin.category.store');
        Route::post('/category/edit/{id}', 'Backend\CategoriesController@update')->name('admin.category.update');
    });

    //Company Routes
    Route::group(['prefix' => '/companies'], function () {
        Route::get('/', 'Backend\CompanyController@index')->name('admin.companies');
        Route::get('/create', 'Backend\CompanyController@create')->name('admin.Company.create');
        Route::get('/company/edit/{id}', 'Backend\CompanyController@edit')->name('admin.Company.edit');
        Route::post('/delete/{id}', 'Backend\CompanyController@delete')->name('admin.Company.delete');
        Route::post('/store', 'Backend\CompanyController@store')->name('admin.Company.store');
        Route::post('/company/edit/{id}', 'Backend\CompanyController@update')->name('admin.Company.update');
    });

    
    // Brand Routes
    Route::group(['prefix' => '/brands'], function(){
        Route::get('/', 'Backend\BrandsController@index')->name('admin.brands');
        Route::get('/create', 'Backend\BrandsController@create')->name('admin.brand.create');
        Route::get('/edit/{id}', 'Backend\BrandsController@edit')->name('admin.brand.edit');

        Route::post('/store', 'Backend\BrandsController@store')->name('admin.brand.store');

        Route::post('/brand/edit/{id}', 'Backend\BrandsController@update')->name('admin.brand.update');
        Route::post('/brand/delete/{id}', 'Backend\BrandsController@delete')->name('admin.brand.delete');
    });

        // Division Routes
    Route::group(['prefix' => '/divisions'], function(){
        Route::get('/', 'Backend\DivisionsController@index')->name('admin.divisions');
        Route::get('/create', 'Backend\DivisionsController@create')->name('admin.division.create');
        Route::get('/edit/{id}', 'Backend\DivisionsController@edit')->name('admin.division.edit');

        Route::post('/store', 'Backend\DivisionsController@store')->name('admin.division.store');

        Route::post('/division/edit/{id}', 'Backend\DivisionsController@update')->name('admin.division.update');
        Route::post('/division/delete/{id}', 'Backend\DivisionsController@delete')->name('admin.division.delete');
    });

    // District Routes
    Route::group(['prefix' => '/districts'], function(){
        Route::get('/', 'Backend\DistrictsController@index')->name('admin.districts');
        Route::get('/create', 'Backend\DistrictsController@create')->name('admin.district.create');
        Route::get('/edit/{id}', 'Backend\DistrictsController@edit')->name('admin.district.edit');

        Route::post('/store', 'Backend\DistrictsController@store')->name('admin.district.store');

        Route::post('/district/edit/{id}', 'Backend\DistrictsController@update')->name('admin.district.update');
        Route::post('/district/delete/{id}', 'Backend\DistrictsController@delete')->name('admin.district.delete');
    });


    
});



Auth::routes();

